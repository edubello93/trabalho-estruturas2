## Instruções
Basta compilar e executar


## Preparação
Planejei fazer uma tabela hash com encadeamento externo para armazenar as palavras. 
Cada palavra seria um nó de uma das listas encadeadas, e quando fosse tentar inserir 
uma palavra que já existia na tabela, um contador no nó iria incrementar. 
Assim seria fácil de saber quantas vezes uma determinada palavra existia no texto da opção
--freq-word.
Para a opção --freq, pensei em implementar um 3-way quicksort e depois correr a lista 
contando quais os itens mais repetidos.


## Execução
Me organizei mal, achando que conseguiria fazer sozinho, mas não consegui. 
Perdi muito tempo implementando a função de normalizar o texto e falhei na 
implementação do resto do sistema, também por dificuldades com a linguagem C e suas peculiaridades.
O projeto, no estado atual, só executa a função de normalizar as palavras.

Desconsidere a maioria dos comentários, pois são restos do debug e da criação do programa.


