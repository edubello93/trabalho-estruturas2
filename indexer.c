#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define tamanhoString 76
/*
    Para indexar as palavras, ser� usada uma tabela hash
    de encadeamento externo. O encadeamento externo �
    feito com listas encadeadas e cada nodo da lista
    corresponde a uma palavra, armazenando tamb�m quantas
    vezes essa palavra teve ocorrencia no texto.
*/

struct node
{
    char* key;
    int quantidade;
    struct node* next;
};

struct hashMap
{
    int numOfElements, capacity;
    struct node** arr;
};


int string_hash(char * s)
{
    char c;
    int p = 31, m = 97;
    int hash_value = 0, p_pow = 1;

    while (c = *s++)
    {
        hash_value = (hash_value +
                      (c - 'a' + 1) * p_pow) % m;
        p_pow = (p_pow * p) % m;
    }
    return(hash_value);
}

int hashFunction(struct hashMap* mp, char* key)
{
    int bucketIndex;
    int sum = 0, factor = 31;
    for (int i = 0; i < strlen(key); i++)
    {

        // sum = sum + (ascii value of
        // char * (primeNumber ^ x))...
        // where x = 1, 2, 3....n
        sum = ((sum % mp->capacity)
               + (((int)key[i]) * factor) % mp->capacity)
              % mp->capacity;

        // factor = factor * prime
        // number....(prime
        // number) ^ x
        factor = ((factor % __INT16_MAX__)
                  * (31 % __INT16_MAX__))
                 % __INT16_MAX__;
    }

    bucketIndex = sum;
    return bucketIndex;
}

void setNode(struct node* node, char* key, int quantidade)
{
    node->key = key;
    node->quantidade = quantidade;
    node->next = NULL;
    return;
};

void initializeHashMap(struct hashMap* mp)
{

    // Default capacity in this case
    mp->capacity = 100;
    mp->numOfElements = 0;

    // array of size = 1
    mp->arr = (struct node**)malloc(sizeof(struct node*)
                                    * mp->capacity);
    return;
}


void insert(struct hashMap* mp, char* key, int quantidade)
{

    // Getting bucket index for the given
    // key - value pair
    int bucketIndex = hashFunction(mp, key);
    struct node* newNode = (struct node*)malloc(

                               // Creating a new node
                               sizeof(struct node));

    // Setting value of node
    setNode(newNode, key, quantidade);

    // Bucket index is empty....no collision
    if (mp->arr[bucketIndex] == NULL)
    {
        mp->arr[bucketIndex] = newNode;
    }

    // Collision
    else
    {

        // Adding newNode at the head of
        // linked list which is present
        // at bucket index....insertion at
        // head in linked list
        newNode->next = mp->arr[bucketIndex];
        mp->arr[bucketIndex] = newNode;
    }
    return;
}


char* search(struct hashMap* mp, char* key)
{

    // Getting the bucket index
    // for the given key
    int bucketIndex = hashFunction(mp, key);

    // Head of the linked list
    // present at bucket index
    struct node* bucketHead = mp->arr[bucketIndex];
    while (bucketHead != NULL)
    {

        // Key is found in the hashMap
        if (bucketHead->key == key)
        {
            bucketHead->quantidade++;
            return bucketHead->quantidade;
        }
        bucketHead = bucketHead->next;
    }

    // If no key found in the hashMap
    // equal to the given key
    char* errorMssg = (char*)malloc(sizeof(char) * 25);
    insert(mp, key, 0);
    return errorMssg;
}


void carregaTextoENormaliza(char *arquiv)
{
    //essa funcao carrega o texto, normaliza linha por linha
    // e joga palavra por palavra maior que 2 digitos

    char palavra[tamanhoString];
    unsigned long int nCaracteres=0,tamArq;
    char x[tamanhoString];
    //int j=0;
    int i = 0,j,l;

    int k, m;
    FILE *arquivo;
    arquivo = fopen(arquiv, "r");
    //arquivo = fopen("10310.txt", "r");
    fseeko(arquivo, 0, SEEK_END);
    tamArq = ftello(arquivo);
    FILE *novo;

    rewind(arquivo);
    //char total[tamArq];

    if(arquivo)
    {
        printf("carregou arquivo\n\n tamanho arquivo: %lu bytes\n",tamArq);
        //printf("\n tamanho arquivo: %lu bytes\n",tamArq);
    }
    else
    {
        printf("Erro: arquivo nao carregado\n\n");
        return 1;
    }
    while(fgets(x,tamanhoString,arquivo))
    {

        for (i = 0; i<tamanhoString; i++)
        {
            if((x[i]>='a')&&(x[i]<='z'))
            {
                x[i]= toupper(x[i]);
            }

            else if ((x[i]<'A'&& x[i]>0)||(x[i]>'Z'))
                //(x[i]>32)
            {
                x[i] = 32;
            }



            nCaracteres++;
        }
//=== tem que fazer depois de arrumar o texto


        for (j=0; j<tamanhoString; j++)
        {
            //printf("\n entrou segundo loop");
            //char palavra[50];
            //printf("\n segundo loop %s\n", palavra);
            //printf("%s\n",x);
            /*
            if(x[j]=='\n')
            {
              break;
            }
            else */if(x[j]>32 && x[j-1]<=32)
            {
                //printf("\n entrou segundo loop if");
                l=j+1;
                while (x[l]!=' ')
                {
                    l++;
                    //printf("j:%d  L:%d\n",j, l);
                }
                if (l>j+2)
                {
                    m=0;
                    for (j; j<=l; j++)
                    {
                        palavra[m] = x[j];
                        m++;
                        //printf("j devia ta 4 mas ta %d", j);
                    }
                    j--;
                    palavra[m]=0;

                    printf("%s", palavra);

                    //aqui tem que passar a palavra pra search mas nao vai

                    //search(mp, palavra);



                    palavra[0]='\0';

                }

            }

        }

        //printf("%s",x);
        memset(x,0,tamanhoString);
        //x[0]='\0';
        //printf("\n\n");

        /*
        x[tamanhoString] = 32;
        strcat(total, x);
        */
        //printf("%s", x);

        //printf("%s               %c\n ",x,x[60]);
        /*
        for(k=0;k<tamanhoString;k++){
            printf("%c ",x[k]);
        }
        printf("\n");
        */
        /*
        if (x[0]>64){
           printf("%s",x);
        }
        */

    }
    //printf("\n\nterminou while\n\n");
    /*
    while
    if (x[i]==' ' && x[i+1]!=' ')
    {
        j=i+1;
        while (x[j]!=' ')
        {
            j++;
        }
        if (j>i+2)
        {
            l=0;
            for (i; i<=j; i++)
            {
                palavra[l] = x[i];
                l++;
            }
        }
    }
    */

    /*
    for(i=0; i<nCaracteres; i++)
    {
        printf("%c",total[i]);
    }
    */
    printf("\n\n%lu caracteres\n\n",nCaracteres);
}



int main (int argc, char *argv[])
{
    //carregaTextoENormaliza();
    printf("\n teste iniciou main ");
    struct hashMap* mp = (struct hashMap*)malloc(sizeof(struct hashMap));
    initializeHashMap(mp);

    int ocorrencia;
    char argumento[50];
    char palavra[50];
    char arquivo[50];

    if (argc == 1)
    {
        //executar somente indexer (normalizacao do texto e indexacao)
        printf("\n teste caiu argc 1 ");
        carregaTextoENormaliza("10310.txt");
        printf("\n teste caiu argc 1 ");
        fclose(arquivo);
    }
    else if (strcmp (argv[1], "--freq") == 0)
    {
        //ocorrencia = argv[2];
        strcpy (ocorrencia, argv[2]);
        strcpy (arquivo, argv[3]);
        printf("\n\n TESTE FREQ palavra:%d \narquivo: %s\n",ocorrencia, arquivo);
        carregaTextoENormaliza(arquivo);
        //busca a palavra e quantas vezes ela ocorreu
    }


    else if (argc == 3)
    {
        if (strcmp (argv[1], "--freq-word") == 0)
        {
            printf("\n\n teste argvs antes 2:%s  3:%s\n",argv[2], argv[3]);
            strcpy (palavra, argv[2]);
            strcpy (arquivo, argv[3]);
            printf("\n\n teste depois palavra:%s \narquivo: %s\n",palavra, arquivo);
            carregaTextoENormaliza(arquivo);
            //busca numero de ocorrencias da palavra
        }
        else if (strcmp (argv[1], "--search") == 0)
        {
            printf("\n\n teste palavra:%s \narquivo: %s\n",palavra, arquivo);
            strcpy (palavra, argv[2]);
            strcpy (arquivo, argv[3]);
            carregaTextoENormaliza(arquivo);
            //busca a palavra no arquivo
            //e calcula a relevancia
        }

    }
    else
    {
        printf ("ARGUMENTO DESCONHECIDO \n");
        strcpy (argumento, "caiu no else");
        return 1;
    }
    //printf("\n teste ocorrencia: %d \n argumento: %s", ocorrencia, argumento);

    fclose(arquivo);
    return 0;
}
